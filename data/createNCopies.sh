#!/usr/bin/env bash

if [[ $# -eq 0 ]] ; then
  echo 'Usage: createNCopies.sh numberOfFileCopies, ex: createNCopies.sh 10'
fi

for ((i=0; i<$1; i++));
do
    echo "Adding times: $i"
    cat book.txt >> $1"book.txt"
done