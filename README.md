# README #

# Start local cluster: #

### Install Spark ###

1. Download distribution from: [spark](https://spark.apache.org/downloads.html)

2. Start local cluster:

```bash                  
./bin/spark-class org.apache.spark.deploy.master.Master
./bin/spark-class org.apache.spark.deploy.worker.Worker spark://10.55.12.48:7077 -c 3 -m 5G 
./bin/spark-class org.apache.spark.deploy.worker.Worker spark://10.55.12.48:7077 -c 3 -m 5G 
./bin/spark-class org.apache.spark.deploy.worker.Worker spark://10.55.12.48:7077 -c 3 -m 5G 
./bin/spark-class org.apache.spark.deploy.worker.Worker spark://10.55.12.48:7077 -c 3 -m 5G 
```

3. check logs 

`tail -f SPARK_HOME/logs/*`

Access Spark Master: [http://10.55.12.48:8080](http://10.55.12.48:8080)

### Using sbin scripts to start Spark? ###

This approach most probably works best if you have an actual cluster of machines where you want to start a cluster.

The SSH daemon is required. As written in this post:

> SSHD is required if you want to use the automated scripts to launch a cluster. It would be cool to add another script that launches a small local cluster that people can use for testing. In my case I usually just manually launch a master and slave, because it's pretty simple.

`sudo systemctl start ssh`

`sbin/start-all.sh`

# Submit a job to the cluster #

```bash
./bin/spark-submit \
  --class org.apache.spark.examples.SparkPi \
  --master spark://10.55.12.48:7077 \
  --executor-memory 5G \
  --total-executor-cores 3 \
  ./examples/jars/spark-examples_2.11-2.2.1.jar \
 100000
```
The 

# Other #

Working based on: https://github.com/datastax/SparkBuildExamples/blob/master/scala/sbt/oss/build.sbt

# Garbage & Backup #

Go to your Spark downloaded installation:

Start Master
./sbin/start-master.sh
Web     : http://localhost:8080/
URL     : spark://oskar-gp:7077
REST URL: spark://oskar-gp:6066 (cluster mode)
less /home/oskar/bin/spark-2.2.1-bin-hadoop2.7/logs/spark-oskar-org.apache.spark.deploy.master.Master-1-oskar-gp.out

./start-slave.sh spark://oskar-gp:7077 -c 6 -m 5g
./sbin/start-slave.sh <master-spark-URL>

# Hadoop as a pseudo-distributed installation #

Im following the process described here: [documentation](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html#Pseudo-Distributed_Operation)

Required components:

1. `sudo apt-get install ssh` is needed only when connecting to remote hosts. 
1. `sudo apt-get install rsync` I don't know what this is used for.
1. Download HADOOP distribution from here: [download](http://ftp.man.poznan.pl/apache/hadoop/common/hadoop-3.0.0/)

1. `sudo systemctl status ssh` start ssh
1. `ssh localhost` check if you can login to localhost without a password
1. If you cannot setup passphraseless ssh
    ```bash  
    $ ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
    $ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
    $ chmod 0600 ~/.ssh/authorized_keys
    ```

1. Update `etc/hadoop/core-site.xml` with:
    ```xml
    <configuration>
        <property>
            <name>fs.defaultFS</name>
            <value>hdfs://localhost:9000</value>
        </property>
    </configuration>
    ```

1. Update `etc/hadoop/hdfs-site.xml` (for pseudo distributed installation security checks will be really annoying, we disable permissions checking):
    ```xml
    <configuration>
       <property>
           <name>dfs.replication</name>
           <value>1</value>
       </property>
       <property>
           <name>dfs.permissions</name>
           <value>false</value>
       </property>
    </configuration>
    ```
    
1. Set JAVA_HOME variable in the file:
    
    `etc/hadoop/hadoop-env.sh`
    
    `export JAVA_HOME=/usr/lib/jvm/java-8-oracle`
   
    
1. Format the filesystem: 
    `$ bin/hdfs namenode -format`
    
1. Start NameNode daemon and DataNode daemon:
    `$ sbin/start-dfs.sh`
    
1. Copy the input files into the distributed filesystem:
    ```bash
    ./hadoop fs -mkdir /gz    
    ./hadoop fs -put ~/crawler/gz/* /gz
    ```    
    
### Hadoop Ports ###

Links:

HDF Link on local host: [explorer](http://0.0.0.0:9870/explorer.html#/)

TODO: make a nice table with links here    
```
Daemon	Default Port	Configuration Parameter
HDFS	Namenode	50070	dfs.http.address
Datanodes	50075	dfs.datanode.http.address
Secondarynamenode	50090	dfs.secondary.http.address
Backup/Checkpoint node?	50105	dfs.backup.http.address
MR	Jobracker	50030	mapred.job.tracker.http.address
Tasktrackers	50060	mapred.task.tracker.http.address
```

# Google Cloud Configuration #

[Google Cloud Dataproc API](https://cloud.google.com/dataproc/docs/reference/rest/?hl=pl)

[Service Discovery](https://dataproc.googleapis.com/$discovery/rest?version=v1)

[This is to analyse later](https://github.com/GoogleCloudPlatform/bigdata-interop/tree/master/gcs)

[Run Spark Scala Job](https://cloud.google.com/dataproc/docs/tutorials/spark-scala)

*Caution: Do not include sensitive information in the bucket name, since the bucket namespace is global and publicly visible.*

After dwonloading and authenticating the Google Clud SDK you can list the buckets like this:
    ```bash
    $ gsutil ls
    gs://dataproc-2fa36c09-7597-4405-a429-bf2f730660fe-europe-west2/
    gs://dataproc-3d59f185-d9e6-4f9e-b9ca-9f74ce4dc63a-europe-west1/
    gs://flightdata/
    
    $ gsutil ls gs://flightdata/
    gs://flightdata/part-00000.txt
    ```

### Write and run Spark Scala jobs on a Cloud Dataproc cluster ###

[DOC](https://cloud.google.com/dataproc/docs/tutorials/spark-scala#using_scala)

# Google API Samples #

[Authenticate for ghe google cloud API](https://cloud.google.com/vision/docs/auth)

POST /v1beta2/projects/bold-zoo-195423/regions/europe-west1/clusters/
```json
{
  "projectId": "bold-zoo-195423",
  "clusterName": "oskar-hdfs-test",
  "config": {
    "configBucket": "",
    "gceClusterConfig": {
      "subnetworkUri": "default"
    },
    "masterConfig": {
      "numInstances": 1,
      "machineTypeUri": "n1-standard-4",
      "diskConfig": {
        "bootDiskSizeGb": 20,
        "numLocalSsds": 0
      }
    },
    "workerConfig": {
      "numInstances": 2,
      "machineTypeUri": "n1-standard-4",
      "diskConfig": {
        "bootDiskSizeGb": 500,
        "numLocalSsds": 0
      }
    }
  }
}
```

Submit a Spark job via REST api to Google Cloud:

`POST /v1/projects/bold-zoo-195423/regions/europe-west1/jobs:submit/`
```json
{
  "projectId": "bold-zoo-195423",
  "job": {
    "placement": {
      "clusterName": "oskar-hdfs-test"
    },
    "reference": {
      "jobId": "b7217977-881b-45d0-a22f-84c9e0be82be"
    },
    "sparkJob": {
      "args": [
        "type=removeDuplicated"
      ],
      "mainClass": "com.itwer.MySparkJob",
      "jarFileUris": [
        "file://C:/mojSzalonyJar.jar"
      ],
      "properties": {
        "key": "value"
      }
    },
    "labels": {
      "color": "red"
    },
    "scheduling": {
      "maxFailuresPerHour": 1
    }
  }
}```


Sample REST response:

```json
{
  "projectId": "bold-zoo-195423",
  "clusterName": "oskar-hdfs-test",
  "config": {
    "configBucket": "dataproc-3d59f185-d9e6-4f9e-b9ca-9f74ce4dc63a-europe-west1",
    "gceClusterConfig": {
      "zoneUri": "https://www.googleapis.com/compute/v1/projects/bold-zoo-195423/zones/europe-west1-b",
      "serviceAccountScopes": [
        "https://www.googleapis.com/auth/bigquery",
        "https://www.googleapis.com/auth/bigtable.admin.table",
        "https://www.googleapis.com/auth/bigtable.data",
        "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
        "https://www.googleapis.com/auth/devstorage.full_control",
        "https://www.googleapis.com/auth/devstorage.read_write",
        "https://www.googleapis.com/auth/logging.write"
      ],
      "subnetworkUri": "https://www.googleapis.com/compute/v1/projects/bold-zoo-195423/regions/europe-west1/subnetworks/default"
    },
    "masterConfig": {
      "numInstances": 1,
      "instanceNames": [
        "oskar-hdfs-test-m"
      ],
      "imageUri": "https://www.googleapis.com/compute/v1/projects/cloud-dataproc/global/images/dataproc-1-2-20180213-070725",
      "machineTypeUri": "https://www.googleapis.com/compute/v1/projects/bold-zoo-195423/zones/europe-west1-b/machineTypes/n1-standard-4",
      "diskConfig": {
        "bootDiskSizeGb": 20
      }
    },
    "workerConfig": {
      "numInstances": 2,
      "instanceNames": [
        "oskar-hdfs-test-w-0",
        "oskar-hdfs-test-w-1"
      ],
      "imageUri": "https://www.googleapis.com/compute/v1/projects/cloud-dataproc/global/images/dataproc-1-2-20180213-070725",
      "machineTypeUri": "https://www.googleapis.com/compute/v1/projects/bold-zoo-195423/zones/europe-west1-b/machineTypes/n1-standard-1",
      "diskConfig": {
        "bootDiskSizeGb": 500
      }
    },
    "softwareConfig": {
      "imageVersion": "1.2.23",
      "properties": {
        "capacity-scheduler:yarn.scheduler.capacity.root.default.ordering-policy": "fair",
        "core:fs.gs.block.size": "134217728",
        "core:fs.gs.metadata.cache.enable": "false",
        "distcp:mapreduce.map.java.opts": "-Xmx819m",
        "distcp:mapreduce.map.memory.mb": "1024",
        "distcp:mapreduce.reduce.java.opts": "-Xmx1638m",
        "distcp:mapreduce.reduce.memory.mb": "2048",
        "hdfs:dfs.datanode.address": "0.0.0.0:9866",
        "hdfs:dfs.datanode.http.address": "0.0.0.0:9864",
        "hdfs:dfs.datanode.https.address": "0.0.0.0:9865",
        "hdfs:dfs.datanode.ipc.address": "0.0.0.0:9867",
        "hdfs:dfs.namenode.http-address": "0.0.0.0:9870",
        "hdfs:dfs.namenode.https-address": "0.0.0.0:9871",
        "hdfs:dfs.namenode.secondary.http-address": "0.0.0.0:9868",
        "hdfs:dfs.namenode.secondary.https-address": "0.0.0.0:9869",
        "mapred:mapreduce.job.maps": "15",
        "mapred:mapreduce.job.reduce.slowstart.completedmaps": "0.95",
        "mapred:mapreduce.job.reduces": "2",
        "mapred:mapreduce.map.cpu.vcores": "1",
        "mapred:mapreduce.map.java.opts": "-Xmx819m",
        "mapred:mapreduce.map.memory.mb": "1024",
        "mapred:mapreduce.reduce.cpu.vcores": "1",
        "mapred:mapreduce.reduce.java.opts": "-Xmx1638m",
        "mapred:mapreduce.reduce.memory.mb": "2048",
        "mapred:mapreduce.task.io.sort.mb": "256",
        "mapred:yarn.app.mapreduce.am.command-opts": "-Xmx819m",
        "mapred:yarn.app.mapreduce.am.resource.cpu-vcores": "1",
        "mapred:yarn.app.mapreduce.am.resource.mb": "1024",
        "spark:spark.driver.maxResultSize": "1920m",
        "spark:spark.driver.memory": "3840m",
        "spark:spark.executor.cores": "1",
        "spark:spark.executor.memory": "2688m",
        "spark:spark.yarn.am.memory": "640m",
        "yarn:yarn.nodemanager.resource.memory-mb": "3072",
        "yarn:yarn.scheduler.maximum-allocation-mb": "3072",
        "yarn:yarn.scheduler.minimum-allocation-mb": "256"
      }
    }
  },
  "status": {
    "state": "RUNNING",
    "stateStartTime": "2018-02-17T22:08:55.937Z"
  },
  "clusterUuid": "311b0500-905e-45d2-be38-56b310a50ad3",
  "statusHistory": [
    {
      "state": "CREATING",
      "stateStartTime": "2018-02-17T22:07:40.625Z"
    }
  ],
  "labels": {
    "goog-dataproc-cluster-name": "oskar-hdfs-test",
    "goog-dataproc-cluster-uuid": "311b0500-905e-45d2-be38-56b310a50ad3",
    "goog-dataproc-location": "europe-west1"
  },
  "metrics": {
    "hdfsMetrics": {
      "dfs-capacity-used": "49152",
      "dfs-capacity-present": "1004692291584",
      "dfs-nodes-decommissioned": "0",
      "dfs-capacity-remaining": "1004692242432",
      "dfs-nodes-decommissioning": "0",
      "dfs-blocks-missing": "0",
      "dfs-blocks-pending-deletion": "0",
      "dfs-capacity-total": "1056621535232",
      "dfs-nodes-running": "2",
      "dfs-blocks-under-replication": "0",
      "dfs-blocks-missing-repl-one": "0",
      "dfs-blocks-corrupt": "0"
    },
    "yarnMetrics": {
      "yarn-apps-failed": "0",
      "yarn-containers-allocated": "0",
      "yarn-memory-mb-allocated": "0",
      "yarn-vcores-pending": "0",
      "yarn-apps-killed": "0",
      "yarn-nodes-lost": "0",
      "yarn-nodes-decommissioned": "0",
      "yarn-nodes-unhealthy": "0",
      "yarn-memory-mb-available": "6144",
      "yarn-containers-reserved": "0",
      "yarn-memory-mb-pending": "0",
      "yarn-memory-mb-total": "-1",
      "yarn-nodes-rebooted": "0",
      "yarn-apps-completed": "0",
      "yarn-containers-pending": "0",
      "yarn-apps-running": "0",
      "yarn-vcores-allocated": "0",
      "yarn-vcores-reserved": "0",
      "yarn-memory-mb-reserved": "0",
      "yarn-vcores-available": "2",
      "yarn-vcores-total": "-1",
      "yarn-apps-submitted": "0",
      "yarn-nodes-active": "2",
      "yarn-apps-pending": "0"
    }
  }
}
```