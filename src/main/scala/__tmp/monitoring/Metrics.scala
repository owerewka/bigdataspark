package __tmp.monitoring

import java.lang.System.currentTimeMillis

import com.codahale.metrics.{Gauge, MetricRegistry}
import __tmp.monitoring.MetricsReporter.startConsoleReport

object Metrics {

  val registry = new MetricRegistry
  val startTime = currentTimeMillis()
  startConsoleReport()

  registry.register("upTimeInMinutes", new Gauge[Integer]() {
    override def getValue = {
      ((currentTimeMillis() - startTime) / (1000 * 60)).toInt
    }
  })

}
