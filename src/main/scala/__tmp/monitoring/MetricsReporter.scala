package __tmp.monitoring

import java.util.concurrent.TimeUnit.{MILLISECONDS, SECONDS}

import com.codahale.metrics._

object MetricsReporter {

  val reporter = ConsoleReporter
    .forRegistry(Metrics.registry)
    .convertRatesTo(SECONDS)
    .convertDurationsTo(MILLISECONDS)
    .build

  def startConsoleReport(): Unit = {
    reporter.start(5, SECONDS)
  }

  def stopConsoleReport(): Unit = {
    reporter.close()
  }

}
