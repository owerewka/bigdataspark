import Config.logFile
import org.apache.spark.sql.SparkSession


object BookScanTest {

  def main(args: Array[String]) {
    val spark = SparkSession
      .builder
      .appName("BookScanTest")
      .getOrCreate()

    val file = spark.read.text(logFile).rdd
    val lines = file.map(_.getAs[String](0)).cache()
    val numAs = lines.filter(_.contains("a")).count()
    val numBs = lines.filter(_.contains("b")).count()
    val numCs = lines.filter(_.contains("c")).count()

    println("Lines with a: %s, Lines with b: %s, Lines with c: %s".format(numAs, numBs, numCs))

    spark.stop()
  }
}

object Config {
  val sparkMaster = "spark://10.55.12.48:7077"
  val sparkHome = "~/bin/spark-2.2.1-bin-hadoop2.7"
  val fatJarFile = "target/scala-2.11/OskarSparkTest-assembly-1.0.jar"
//  val logFile = "/home/oskar/projects/bigdataspark/data/book.txt"
  val logFile = "hdfs://localhost:9000/book.txt"
}


//read.text("hdfs://localhost:9000/book.txt").rdd