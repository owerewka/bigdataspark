import org.apache.spark.sql.SparkSession

/*

This is how to submit this job to Google Cloud:

POST /v1/projects/bold-zoo-195423/regions/global/jobs:submit/
{
  "projectId": "bold-zoo-195423",
  "job": {
    "placement": {
      "clusterName": "cluster-1d73"
    },
    "reference": {
      "jobId": "8b25c7cd-de41-4825-9cdc-fdd4ef27ae08"
    },
    "sparkJob": {
      "mainClass": "GCloudFlightTest",
      "jarFileUris": [
        "gs://flightdata/OskarSparkTest-assembly-1.0.jar"
      ]
    }
  }
}

gcloud dataproc jobs wait b98d19f8-128b-4bea-a245-701987fe5195 --project bold-zoo-195423

 */


object GCloudFlightTest {

  def main(args: Array[String]) {

    val spark = SparkSession
      .builder
      .appName("BookScanTest")
      .getOrCreate()

    val sc = spark.sparkContext

    val text_file = sc.textFile("gs://flightdata/1000book.txt")
    val wordCounts = text_file.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey((a, b) => a + b)
    val sorted = wordCounts.collect.sorted

    sorted.foreach(println(_))

    spark.stop()
  }
}

//text_file.map(_.replace('0','X')).saveAsTextFile("gs://flightdata/part-00000.noZeroes.txt")
//read.text("hdfs://localhost:9000/book.txt").rdd