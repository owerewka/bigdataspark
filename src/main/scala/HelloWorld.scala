

object HelloWorld {

  implicit def intWithTimes(n: Int) = new {
    def times(f: => Unit) = 1 to n foreach (_ => f)
  }

  def main(args: Array[String]): Unit = {
    10 times {
      println("Hello World!")
    }
  }
}

