import com.google.cloud.storage.{Storage, StorageOptions}
import java.nio.charset.StandardCharsets.UTF_8
import com.google.cloud.storage.{BlobId, BlobInfo, BucketInfo}

//requires IntelliJ 2017.3.4
object Upload extends App {


  val storage: Storage = StorageOptions.getDefaultInstance.getService

  // Create a bucket// Create a bucket

  val bucketName = "flightdata" // Change this to something unique

  storage.create(BucketInfo.of(bucketName))

  // Upload a blob to the newly created bucket
  val blobId: BlobId = BlobId.of(bucketName, "my_blob_name")
  val blobInfo: BlobInfo = BlobInfo.newBuilder(blobId).setContentType("text/plain").build
  storage.create(blobInfo, "a simple blob".getBytes(UTF_8))


  //Try to follow this tutorial:
  // https://github.com/GoogleCloudPlatform/google-cloud-java/tree/master/google-cloud-storage


  //  val bucket = storage.create(BucketInfo.newBuilder(bucketName)
  //    // See here for possible values: http://g.co/cloud/storage/docs/storage-classes
  //    .setStorageClass(StorageClass.COLDLINE)
  //    // Possible values: http://g.co/cloud/storage/docs/bucket-locations#location-mr
  //    .setLocation("asia")
  //    .build());


  //  def createBucket(bucketName : String)
  //  {
  //    var storage = StorageClient.Create();
  //    storage.CreateBucket(s_projectId, bucketName);
  //    Console.WriteLine($"Created {bucketName}.");
  //  }


}
