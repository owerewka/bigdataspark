package domain.files

import java.io.File

class FileList(directory: String) {

  val list: Seq[String] = getListOfFiles(directory)

  private def getListOfFiles(directory: String): Seq[String] = {
    val dir = new File(directory)
    if (dir.exists && dir.isDirectory) {
      dir.listFiles
        .filter(_.isFile)
        .map(file => file.getCanonicalPath)
        .toList
    } else {
      Seq.empty[String]
    }
  }

  //    import java.io.File
  //    val data = sc.textFile(files.mkString(","))

}

object FileList {
  def apply(directory: String): FileList = new FileList(directory)
}