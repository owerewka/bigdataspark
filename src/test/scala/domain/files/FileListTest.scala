package domain.files

import java.io.File

import domain.files.FileListTest.prefix
import org.scalatest.{FlatSpec, Matchers}

class FileListTest extends FlatSpec with Matchers {

  "A FileList" should "read content of different directory" in {
    val files = FileList("./src/test/resources/files/onefile").list
    files should have size (1)
    files should contain(prefix("/src/test/resources/files/onefile/file.txt"))
  }

  "A FileList" should "provide the list of files" in {
    val files = FileList("./src/test/resources/files/threefiles").list
    files should have size (3)
    files should contain(prefix("/src/test/resources/files/threefiles/1.txt"))
    files should contain(prefix("/src/test/resources/files/threefiles/2.txt"))
    files should contain(prefix("/src/test/resources/files/threefiles/3.txt"))
  }

  "A FileList" should "return empty Seq for a directory with no files inside" in {
    FileList("./src/test/resources/files/empty").list shouldBe empty
  }
}

object FileListTest {
  def prefix(path: String): String = {
    new File(".").getCanonicalPath + path
  }
}