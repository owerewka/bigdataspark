#!/usr/bin/env bash

./bin/spark-submit \
  --class org.apache.spark.examples.SparkPi \
  --master spark://10.55.12.48:7077 \
  --executor-memory 5G \
  --total-executor-cores 3 \
  ./examples/jars/spark-examples_2.11-2.2.1.jar \
 100000