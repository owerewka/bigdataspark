#!/usr/bin/env bash

~/bin/spark-2.2.1-bin-hadoop2.7/bin/spark-submit \
  --class BookScanTest \
  --master spark://10.55.12.48:7077 \
  --executor-memory 5G \
  --total-executor-cores 3 \
  /home/oskar/projects/bigdataspark/target/scala-2.11/OskarSparkTest-assembly-1.0.jar