#!/usr/bin/env bash

echo "Requires bash 4"

for i in {00..29};
do
    url="https://infare-dev-external-dpcandidate.s3.amazonaws.com/hive_csv_altus_gz/part-000$i.gz"
    wget -P ~/Desktop/delete/ $url
done